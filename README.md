# 1. Uvod

[Desktop Operating System Market Share Worldwide | Statcounter Global Stats](https://gs.statcounter.com/os-market-share/desktop/worldwide/)

| Pozicija  | oblik                                                                                                                      |
| --------- | -------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | ("Statcounter", bez dat.)                                                                                                  |
| Drugi Put | ("Statcounter", bez dat.)                                                                                                  |
| popis     | Statcounter (bez dat.) OS Marketshare. Preuzeto 10.9.2022. s https://gs.statcounter.com/os-market-share/desktop/worldwide/ |

[Operating system market share](https://www.netmarketshare.com/operating-system-market-share.aspx?options=%7B%22filter%22%3A%7B%22%24and%22%3A%5B%7B%22deviceType%22%3A%7B%22%24in%22%3A%5B%22Desktop%2Flaptop%22%5D%7D%7D%5D%7D%2C%22dateLabel%22%3A%22Trend%22%2C%22attributes%22%3A%22share%22%2C%22group%22%3A%22platform%22%2C%22sort%22%3A%7B%22share%22%3A-1%7D%2C%22id%22%3A%22platformsDesktop%22%2C%22dateInterval%22%3A%22Monthly%22%2C%22dateStart%22%3A%222019-11%22%2C%22dateEnd%22%3A%222020-10%22%2C%22segments%22%3A%22-1000%22%7D)

| Pozicija  | oblik                                                                                                          |
| --------- | -------------------------------------------------------------------------------------------------------------- |
| Prvi put  | ("Netmarketshare", bez dat.)                                                                                   |
| Drugi Put | ("Netmarketshare", bez dat.)                                                                                   |
| popis     | Netmarketshare (bez dat.) Operating System Market Share. Preuzeto 10.9.2022. s https://www.netmarketshare.com/ |

# 2. Problemi distribucije aplikacija

- [x] Compatibility

- [x] Dependancies

- [x] Fragmentacija distribucija

- [x] Futureproofing

# 3. Načini pakiranja

## 3.1 Nativni načini pakiranja

### 3.1.1. apt/deb

- [x] Official repo VS ppa
- [x] ppa
- [x] apt
- [x] deb
- [x] CLI/store

    [What is PPA? Everything You Need to Know About PPA in Linux](https://itsfoss.com/ppa-guide/#:~:text=The%20PPA%20allows%20application%20developers,via%20the%20official%20Ubuntu%20repositories.)

| Pozicija  | oblik                                                                                                               |
| --------- | ------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | (Prakash, 2021)                                                                                                     |
| Drugi Put | (Prakash, 2021)                                                                                                     |
| popis     | Prakash, A. (2021). Using PPA in Ubuntu Linux [Complete Guide] Preuzeto 10.9.2022. s https://itsfoss.com/ppa-guide/ |

### 3.1.2. dnf/rpm

- [x] Kako radi dnf

- [ ] apt vs dnf

[Package management system :: Fedora Docs](https://docs.fedoraproject.org/en-US/quick-docs/package-management/)

| Pozicija  | oblik                                                                                                                                           |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | ("Package management system", bez dat.)                                                                                                         |
| Drugi Put | ("Package management system", bez dat.)                                                                                                         |
| popis     | Package management stystem (bez dat.). U Fedora Docs. Preuzeto 10.9.2022. s https://docs.fedoraproject.org/en-US/quick-docs/package-management/ |

### 3.1.3 AUR

- [x] unofficial

- [ ] povijest

- [x] koristenje

- [x] primjer

- [x] PKGBUILD

- [x] Git repo

- [x] Source nije na AUR serverima

[Arch User Repository - ArchWiki](https://wiki.archlinux.org/title/Arch_User_Repository#:~:text=The%20Arch%20User%20Repository%20(AUR,then%20install%20it%20via%20pacman.)

| Pozicija  | oblik                                                                                                                    |
| --------- | ------------------------------------------------------------------------------------------------------------------------ |
| Prvi put  | ("Arch user repository", bez dat.)                                                                                       |
| Drugi Put | ("Arch user repository", bez dat.)                                                                                       |
| popis     | Arch user repository (bez dat.). U ArchWiki. Preuzeto 10.9.2022. s https://wiki.archlinux.org/title/Arch_User_Repository |

[Arch User Repository: How Does It Really Work - YouTube](https://www.youtube.com/watch?v=VbsmQFNx5n4)

| Pozicija  | oblik                                                                                                                                                        |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Prvi put  | (Robertson, 2021)                                                                                                                                            |
| Drugi Put | (Robertson (2021))                                                                                                                                           |
| popis     | Robertson, B., (01.03.2021.). Arch User Repository: How Does It Really Work [Video file]. Preuzeto 10.09.2022. s https://www.youtube.com/watch?v=VbsmQFNx5n4 |

## 3.2. Cross-platform / moderni načini pakiranja

[Flatpak vs Snaps vs Appimage vs Packages - Linux packaging formats compared - YouTube](https://www.youtube.com/watch?v=9HuExVD56Bo)

| Pozicija  | oblik                                                                                                                                                                                 |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | (Stouff (2020))                                                                                                                                                                       |
| Drugi Put | (Stouff (2020))                                                                                                                                                                       |
| popis     | Stouff, N. (23.06.2020). Flatpak vs Snaps vs Appimage vs Packages - Linux packaging formats compared [Video file]. Preuzeto 10.09.2022. s https://www.youtube.com/watch?v=9HuExVD56Bo |

### 3.2.1 Appimage

- [x] Open source

- [x] Za update potreban redownload i "reinstalacija"

- [x] Kompatibilno sa svime

- [x] Appimagehub.com 1100+ aplikacija

- [x] Slično portabilnim aplikacijama na windowsima il dmgu na macu

- [x] Neograničen backwards compatibility

- [x] Futureproofed af

- [x] Veliko zauzeće na disku (primjer gimp)

- [x] integrator app

[Releases · aferrero2707/gimp-appimage · GitHub](https://github.com/aferrero2707/gimp-appimage/releases)

| Pozicija  | oblik |
| --------- | ----- |
| Prvi put  |       |
| Drugi Put |       |
| popis     |       |

[https://appimage.org/](https://appimage.org/)

| Pozicija  | oblik |
| --------- | ----- |
| Prvi put  |       |
| Drugi Put |       |
| popis     |       |

[https://www.appimagehub.com/](https://www.appimagehub.com/)

| Pozicija  | oblik                                                                     |
| --------- | ------------------------------------------------------------------------- |
| Prvi put  | (Appimagehub.com, bez dat.)                                               |
| Drugi Put | (Appimagehub.com, bez dat.)                                               |
| popis     | Appimagehub.com (bez dat.) Preuzeto 10.09.2022. s https://appimagehub.com |

### 3.2.2 Snap

- [x] Canonical

- [x] Autoupdate

- [x] Ubuntu

- [x] Closed-source store

- [ ] Sporo otvaranje

- [x] Dobar za servere

- [x] odbijen od strane Minta, ne nužno prihvaćen

- [x] SnapStore

[Getting started | Snapcraft documentation](https://snapcraft.io/docs/getting-started)

| Pozicija  | oblik                                                                                                                    |
| --------- | ------------------------------------------------------------------------------------------------------------------------ |
| Prvi put  | ("Getting started", bez dat.)                                                                                            |
| Drugi Put | ("Getting started", bez dat.)                                                                                            |
| popis     | Getting started. (bez dat.). U Snapcraft documentation. Preuzeto 10.09.2022. s https://snapcraft.io/docs/getting-started |

[An Introduction To Snaps - YouTube](https://www.youtube.com/watch?v=R6wrP-QEI3k)

| Pozicija  | oblik                                                                                                                               |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | (Dandrea, 2019)                                                                                                                     |
| Drugi Put | (Dandrea, 2019)                                                                                                                     |
| popis     | Dandrea, E. (21.01.2019). An Introduction To Snaps [Video file]. Preuzeto 10.09.2022. s https://www.youtube.com/watch?v=R6wrP-QEI3k |

### 3.2.3. Flatpak

- [x] FlatSeal

- [x] Dijele runtime

- [ ] As fast as native

- [x] CrossPlatform

- [x] Sigurnosni aspekt

- [x] Flathub

- [x] SteamDeck

[FLATPAK is the FUTURE of LINUX applicattion distribution - YouTube](https://www.youtube.com/watch?v=zs9QpPKDw74&)

| Pozicija  | oblik                                                                                                                                                             |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | (Stouff, 2022)                                                                                                                                                    |
| Drugi Put | (Stouff, 2022)                                                                                                                                                    |
| popis     | Stouff, N. (19.01.2022). FLATPAK is the FUTURE of LINUX application distribution [Video file]. Preuzeto 10.09.2022. s ttps://www.youtube.com/watch?v=zs9QpPKDw74& |

[Steam also goes to Flatpak | Linux Addicts](https://www.linuxadictos.com/en/steam-tambien-se-pasa-al-flatpak.html)

| Pozicija  | oblik |
| --------- | ----- |
| Prvi put  |       |
| Drugi Put |       |
| popis     |       |

[Flatpak &#8211; a look behind the portal | Goings on](https://blogs.gnome.org/mclasen/2018/07/19/flatpak-a-look-behind-the-portal/#:~:text=Flatpak%20allows%20sandboxed%20apps%20to,to%20expose%20to%20untrusted%20apps.)

| Pozicija  | oblik                                                                                                                                                               |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | (McLasen, 2018)                                                                                                                                                     |
| Drugi Put | (McLasen, 2018)                                                                                                                                                     |
| popis     | McLasen (2018). Flatpak - a look behind the portal [Blog post]. Preuzeto 10.09.2022. s https://blogs.gnome.org/mclasen/2018/07/19/flatpak-a-look-behind-the-portal/ |

[Flathub—An app store and build service for Linux](https://flathub.org/apps/details/com.github.tchx84.Flatseal)

| Pozicija  | oblik                                                                                                 |
| --------- | ----------------------------------------------------------------------------------------------------- |
| Prvi put  | ("Flatseal", 2022)                                                                                    |
| Drugi Put | ("Flatseal", 2022)                                                                                    |
| popis     | "Flatseal" (2022). Preuzeto 10.09.2022. s https://flathub.org/apps/details/com.github.tchx84.Flatseal |

# 4. Usporedba

## 4.1. Veličina paketa

- [ ] Appimage najveci
- [ ] Snap
- [ ] Flatpak - prikazat razliku izmedu friske i ne friske instalacije
- [ ] Native najmanji

## 4.2. Brzina pokretanja

###### Primjer GIMP

PRVO OTVARANJE
2713ms - snap
1696ms - flatpak
1495ms - rpm

DRUGO OTVARANJE
1524ms - snap
1108ms - flatpak
1050ms - rpm

## 4.3 JavaScript benchmark

[HUGE Speed Differences! Flatpak vs. Snap vs. AppImage - YouTube](https://www.youtube.com/watch?v=OftD86RgAcc)

| Pozicija  | oblik |
| --------- | ----- |
| Prvi put  |       |
| Drugi Put |       |
| popis     |       |

## 4.4 Video rendering benchmark

[HUGE Speed Differences! Flatpak vs. Snap vs. AppImage - YouTube](https://www.youtube.com/watch?v=OftD86RgAcc)

| Pozicija  | oblik |
| --------- | ----- |
| Prvi put  |       |
| Drugi Put |       |
| popis     |       |

# 5. Distribucija vlastite aplikacije

## 5.1 Pregled aplikacije

[Elektronforge](%5Bhttps://www.electronforge.io/%5D(https://www.electronforge.io/))

| Pozicija  | oblik                                                                            |
| --------- | -------------------------------------------------------------------------------- |
| Prvi put  | (ElectronForge.io, bez dat.)                                                     |
| Drugi Put | (ElectronForge.io, bez dat.)                                                     |
| popis     | Electronforge.io (bez dat.) Preuzeto 10.09.2022. s https://www.electronforge.io/ |

[ElectronJS](https://www.electronjs.org/)

- [x] Što app radi

- [x] Kako app radi

| Pozicija  | oblik                                                                    |
| --------- | ------------------------------------------------------------------------ |
| Prvi put  | (ElectronJS.org, bez dat.)                                               |
| Drugi Put | (ElectronJS.org, bez dat.)                                               |
| popis     | ElectronJS.org (bez dat.). Preuzeto 10.09.2022. s https://electronjs.org |

## 5.2 Proces pakiranja

[Electronforge](%5Bhttps://www.electronforge.io/%5D(https://www.electronforge.io/))

[ElectronJS](https://www.electronjs.org/)

- [x] Problem pri pakiranju

- [x] Proces pakiranja

- [x] Konfiguracija

- [x] Webpack

- [x] React

[Zypak module error when running Flatpaks · Issue #2805 · electron-userland/electron-forge · GitHub](https://github.com/electron-userland/electron-forge/issues/2805)

| Pozicija  | oblik |
| --------- | ----- |
| Prvi put  |       |
| Drugi Put |       |
| popis     |       |

## 5.3 Postavljanje na repozitorije

[How to Create Your Own Repositories for Packages - Percona Database Performance Blog](https://www.percona.com/blog/2020/01/02/how-to-create-your-own-repositories-for-packages/)

| Pozicija  | oblik                                                                                                                                                                                             |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | (Patlan, 2020)                                                                                                                                                                                    |
| Drugi Put | (Patlan, 2020)                                                                                                                                                                                    |
| popis     | Patlan, E. (2020). How to create your own repositories for packages [Blog post]. Preuzeto 10.09.2022. s https://www.percona.com/blog/2020/01/02/how-to-create-your-own-repositories-for-packages/ |

### 5.3.1 Debian (deb ppa)

- [x] done

[How to Create Your Own Repository for Packages on Debian](https://linuxopsys.com/topics/create-your-own-repository-for-packages-on-debian)

| Pozicija  | oblik                                                                                                                                                                   |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | ("How to Create Your Own Repository for Packages on Debian", 2022)                                                                                                      |
| Drugi Put | ("How to Create Your Own Repository for Packages on Debian", 2022)                                                                                                      |
| popis     | How to Create Your Own Repository for Packages on Debian (2022). Preuzeto 10.09.2022. s https://linuxopsys.com/topics/create-your-own-repository-for-packages-on-debian |

Izvor za sliku: https://launchpad.net/~gnumdk/+archive/ubuntu/lollypop

| Pozicija  | oblik |
| --------- | ----- |
| Prvi put  |       |
| Drugi Put |       |
| popis     |       |

### 5.3.2 Fedora (rpm dnf)

- [x] done

[VSCodium - Open Source Binaries of VSCode](https://vscodium.com/#install)

| Pozicija  | oblik                                                                              |
| --------- | ---------------------------------------------------------------------------------- |
| Prvi put  | ("VSCodium", bez dat.)                                                             |
| Drugi Put | ("VSCodium", bez dat.)                                                             |
| popis     | VSCodium (bez dat.) VSCodium. Preuzeto 10.09.2022. s https://vscodium.com/#install |

### 5.3.3 Flatpak

- [x] Dodatne datoteke

- [x] Python skripta

- [x] Nedostatna dokumentacija (reddit reference)

[USB Drives &mdash; Flatpak documentation](https://docs.flatpak.org/en/latest/usb-drives.html)

| Pozicija  | oblik                                                                                                                      |
| --------- | -------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | ("Publishing >> USB Drives", bez dat.)                                                                                     |
| Drugi Put | ("Publishing >> USB Drives", bez dat.)                                                                                     |
| popis     | Publishing. (bez dat.). U Flatpak documentation. Preuzeto 10.09.2022. s https://docs.flatpak.org/en/latest/usb-drives.html |

[App Submission · flathub/flathub Wiki · GitHub](https://github.com/flathub/flathub/wiki/App-Submission)

| Pozicija  | oblik                                                                                                                      |
| --------- | -------------------------------------------------------------------------------------------------------------------------- |
| Prvi put  | ("App submission", bez dat.)                                                                                               |
| Drugi Put | ("App submission", bez dat.)                                                                                               |
| popis     | App submission. (bez dat.). U Flathub wiki. Preuzeto 10.09.2022. s https://github.com/flathub/flathub/wiki/App-Submissionv |

[GitHub - flathub/com.bitwarden.desktop](https://github.com/flathub/com.bitwarden.desktop)

| Pozicija  | oblik |
| --------- | ----- |
| Prvi put  |       |
| Drugi Put |       |
| popis     |       |

# 6. Zaključak

- [x] Native paketi će i dalje biti korištni za core komponente

- [x] Korisnici će sve više koristiti Flatpak
  
  - [x] SteamOS
  
  - [x] Endless OS
  
  - [x] Fedora?

- [x] Snap, iako ne široko prihvaćen na desktopu osim na Ubuntu postaje sve bitniji akter na serverskoj strani

- [x] Moderni sustavi pakiranja kao i međuplatformne programerske platforme potaknut će i olakšati izradu aplikacija za GNU/Linux sustave te ih dovesti na razinu Windowsa i MacOS-a.
